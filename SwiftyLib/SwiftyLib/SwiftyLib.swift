//
//  SwiftyLib.swift
//  
//
//  Created by Mert Kahvecioğlu on 10.10.2022.
//

import Foundation

public final class SwiftyLib {

    let name = "SwiftyLib"
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
